#!/bin/bash

#   HTPC setup script, lifted heavily from https://github.com/tom472/mediabox\
#   and Integrated with phikai's htpc-docker-standup stack setup

# Check that script was run not as root or with sudo
if [ "$EUID" -eq 0 ]
  then echo "Please do not run this script as root or using sudo"
  exit
fi

#set -x

# TODO: 
# Figure out user/pass for each system and if necessary

# See if we need to check GIT for updates
if [ -e .env ]; then
#    Stash any local changes to the base files
#    git stash > /dev/null 2>&1
#    printf "Updating your local copy of htpc-docker-standup.\\n\\n"
#    # Pull the latest files from Git
#    git pull
#    # Check to see if this script "setup.sh" was updated and restart it if necessary
#    changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
#    check_run() {
#        echo "$changed_files" | grep --quiet "$1" && eval "$2"
#    }
#    # Provide a message once the Git check/update is complete
#    if [ -z "$changed_files" ]; then
#        printf "Your htpc-docker-standup is current - No Update needed.\\n\\n"
#    else
#        printf "All htpc-docker-standup files updated.\\n\\nThis script will restart if necessary\\n\\n"
#    fi
#    # Rename the .env file so this check fails if htpc-docker-standup.sh needs to re-launch
    mv .env 1.env
    echo "### 
    ###  -----------------------------------------------------
    ###  H T P C - D O C K E R   C O N F I G   S E T T I N G S
    ###  -----------------------------------------------------
    ###    The values configured here are applied during
    ###    $ docker-compose up -d
    ###  -----------------------------------------------------
    ###    Welcome to the setup script. Please follow all of 
    ###    the prompts until the script completes. GLHF!
    ###  -----------------------------------------------------
    ###"
    read -r -p "Press any key to continue... " -n1 -s
    printf "\\n\\n"
#    # Run exec setup.sh if setup.sh changed
#    check_run setup.sh "exec ./setup.sh"
fi

# After update collect some current known variables
if [ -e 1.env ]; then
    # Grab the CouchPotato, NBZGet, & PIA usernames & passwords to reuse
    DAEMONNAME=$(grep DAEMONNAME 1.env | cut -d = -f2)
    DAEMONPASS=$(grep DAEMONPASS 1.env | cut -d = -f2)
    VPNUNAME=$(grep VPNUNAME 1.env | cut -d = -f2)
    VPNPASS=$(grep VPNPASS 1.env | cut -d = -f2)
    VPN_REMOTE=
    DLDIRECTORY=$(grep DLDIRECTORY 1.env | cut -d = -f2)
    TVDIRECTORY=$(grep TVDIRECTORY 1.env | cut -d = -f2)
    MOVIEDIRECTORY=$(grep MOVIEDIRECTORY 1.env | cut -d = -f2)
    MUSICDIRECTORY=$(grep MUSICDIRECTORY 1.env | cut -d = -f2)
    PMSTAG=$(grep PMSTAG 1.env | cut -d = -f2)
    PMSTOKEN=$(grep PMSTOKEN 1.env | cut -d = -f2)
    TRAEFIK_AUTH=$(grep TRAEFIK_AUTH 1.env | cut -d = -f2)
    STACK_NAME=$(grep STACK_NAME 1.env | cut -d = -f2)
    WATCHTOWER_EMAIL=$(grep WATCHTOWER_EMAIL 1.env | cut -d = -f2)
    SMTP_FROM=$(grep SMTP_FROM 1.env | cut -d = -f2)
    SMTP_SERVER=$(grep SMTP_SERVER 1.env | cut -d = -f2)
    SMTP_PORT=$(grep SMTP_PORT 1.env | cut -d = -f2)
    SMTP_USER=$(grep SMTP_USER 1.env | cut -d = -f2)
    SMTP_PASS=$(grep SMTP_PASS 1.env | cut -d = -f2)
    EMAIL=$(grep EMAIL 1.env | cut -d = -f2)
    DOMAIN=$(grep DOMAIN 1.env | cut -d = -f2)
    # Echo back the media directories, and other info to see if changes are needed
    printf "These are the Media Directory paths currently configured.\\n"
    printf "Your DOWNLOAD Directory is: %s \\n" "$DLDIRECTORY"
    printf "Your TV Directory is: %s \\n" "$TVDIRECTORY"
    printf "Your MOVIE Directory is: %s \\n" "$MOVIEDIRECTORY"
    printf "Your MUSIC Directory is: %s \\n" "$MUSICDIRECTORY"
    read  -r -p "Do you need to change these directories? (y/n) " DIRANSWER `echo \n`
    read  -r -p "Do you need to change your Plex Credentials? (y/n) " PLEXANSWER `echo \n`
    read  -r -p "Do you need to change your PIA Credentials? (y/n) " VPNANSWER `echo \n`
    read  -r -p "Do you need to change your info used for Let's Encrypt? (y/n) " EMAILANSWER `echo \n`
    read  -r -p "Do you need to change the Watchtower notification address? (y/n) " WATCHANSWER `echo \n`
    read  -r -p "Do you need to change the SMTP Settings? (y/n) " SMTPANSWER `echo \n`
    read  -r -p "Do you need to change the traefik authentication? (y/n) " TRAEANSWER `echo \n`
    # Now we need ".env" to exist again so we can stop just the containers
    mv 1.env .env
    # Stop the current htpc-docker-standup stack
    printf "\\n\\nStopping Current htpc-docker-standup containers.\\n\\n"
    docker-compose stop
    # Make a datestampted copy of the existing .env file
    mv .env "$(date +"%Y-%m-%d_%H:%M").env"
fi

#   Setup using the .env file requirements
# Get local Username
LOCALUSER=$(id -u -n)
# Get Hostname
HOSTNAME=$(hostname)
# Get IP Address
IP_ADDRESS=$(hostname -I | awk '{print $1}')
# Get PUID
PUID=$(id -u "$LOCALUSER")
# Get GUID
PGID=$(id -g "$LOCALUSER")
# Get info needed for Plex Official image
if [[ -z "$PMSTOKEN" || -z "$PLEXANSWER" || "$PLEXANSWER" == "y" ]]; then
    read -r -p "Which Plex release do you want to run? By default 'public' will be used. (latest, public, plexpass): " PMSTAG
    read -r -p "If you have PlexPass what is your Claim Token from https://www.plex.tv/claim/ (Optional): " PMSTOKEN
    # If not set - set PMS Tag to Public:
    if [ -z "$PMSTAG" ]; then
        PMSTAG=public
    fi
fi

# Where would you like to store configs?
echo "Where do you want to store your docker configs?"
read -r -p "Please use full path - /path/to/downloads (default will be nested within the script directory): " CONFIGDIR
if [[ -z $CONFIGDIR ]];then
    CONFIGDIR=$(pwd)/config
fi

# set default to current dir
mkdir -p $CONFIGDIR/delugevpn/config/openvpn
mkdir -p $CONFIGDIR/duplicati/backups
mkdir -p $CONFIGDIR/historical/env_files
mkdir -p $CONFIGDIR/jackett
mkdir -p $CONFIGDIR/lidarr
mkdir -p $CONFIGDIR/nzbget
mkdir -p $CONFIGDIR/ombi
mkdir -p $CONFIGDIR/"plex/Library/Application Support/Plex Media Server/Logs"
mkdir -p $CONFIGDIR/portainer
mkdir -p $CONFIGDIR/radarr
mkdir -p $CONFIGDIR/sonarr
mkdir -p $CONFIGDIR/tautulli

cp ovpn/*/*.crt $CONFIGDIR/delugevpn/config/openvpn/ > /dev/null 2>&1
cp ovpn/*/*.pem $CONFIGDIR/delugevpn/config/openvpn/ > /dev/null 2>&1

# Setup PIA settings //TODO Allow for selections and handle appropriately with config files
VPNPROVIDER=pia
# Get PIA User Info
if [[ -z "$VPNUNAME" || -z "$VPNANSWER" || "$VPNANSWER" == "y" ]]; then
    read -r -p "What is your PIA Username?: " VPNUNAME
    read -r -s -p "What is your PIA Password? (Will not be echoed): " VPNPASS
    printf "\\n\\n"
    VPNLIST="$(ls ./ovpn/$VPNPROVIDER | grep ".ovpn" | awk 'BEGIN { FS = "." } ; {print $1}')"
    select FILENAME in $VPNLIST;
    do
        echo "You picked $FILENAME ($REPLY), so that's what we will use"
        VPN_REMOTE="$(cat ./ovpn/$VPNPROVIDER/$FILENAME.ovpn | grep -m 1 remote | awk '{print $2}')"
        cp ./ovpn/$VPNPROVIDER/$FILENAME.ovpn $CONFIGDIR/delugevpn/config/openvpn/ > /dev/null 2>&1
        break
    done
fi

# Get CIDR Address
SLASH=$(ip a | grep "$IP_ADDRESS" | cut -d ' ' -f6 | awk -F '/' '{print $2}')
CIDR_ADDRESS=$(awk -F"." '{print $1"."$2"."$3".0"}'<<<$IP_ADDRESS)/$SLASH
# Get Time Zone
TZ=$(cat /etc/timezone)	

# Get Docker Group Number
DOCKERGRP=$(grep docker /etc/group | cut -d ':' -f 3)

# Let's Encrypt setup
if [[ -z "$EMAIL" || -z "$EMAILANSWER" || "$EMAILANSWER" == "y" ]]; then
    printf "\\n\\n"
    printf "Let's Encrypt domain setup::"
    printf "\\n\\n"
    read -r -p "What email address would you like to use for Let's Encrypt? (someone@somewhere.com): " EMAIL
    read -r -p "What public domain should be used for accessing services? (domain.com): " DOMAIN
fi
# Watchtower setup
if [[ -z "$WATCHTOWER_EMAIL" || -z "$WATCHANSWER" || "$WATCHANSWER" == "y" ]]; then
    printf "\\n\\n"
    printf "Watchtower notification setup::"
    printf "\\n\\n"
    read -r -p "What email address would you like to use for Watchtower notifications? (someone@somewhere.com): " WATCHTOWER_EMAIL
fi
#SMTP setup
if [[ -z "$SMTP_USER" || -z "$SMTPANSWER" || "$SMTPANSWER" == "y" ]]; then
    printf "\\n\\n"
    printf "SMTP Server setup::"
    printf "\\n\\n"
    read -r -p "What from address does your SMTP server use to send email? (someone@somewhere.com): " SMTP_FROM
    read -r -p "What is the servername for your SMTP server? (smtp.domain.com): " SMTP_SERVER
    read -r -p "What port does your SMTP server use to connect? (587): " SMTP_PORT
    read -r -p "What username does your SMTP server use to authenticate: " SMTP_USER
    read -r -s -p "What password does your SMTP use to authenticate: " SMTP_PASS
fi

#SPEEDTEST_INTERVAL= - Number of seconds between tests to the Speedtest.net service
SPEEDTEST_INTERVAL=300

#TRAEFIK_AUTH= - Basic Auth for the Traefik Admin htpasswd Generator
if [[ -z "$TRAEFIK_AUTH" || -z "$TRAEANSWER" || "$TRAEANSWER" == "y" ]]; then
    printf "\\n\\n"
    printf "Traefik authentication setup::"
    printf "\\n\\n"
    read -r -p "What username would you like to use to authenticate with traefik: " TRAE_USER
    read -r -s -p "Enter the password to use with traefik: " TRAE_PASS1
    TRAEFIK_AUTH=$(htpasswd -nb $TRAE_USER $TRAE_PASS1)
fi

#STACK_NAME= - This is used to specify the appropriate network Traefik should use. See #13 for details.
if [[ -z "$STACK_NAME" || -z "$STACKANS" || "$STACKANS" == "y" ]]; then
    printf "\\n\\n"
    printf "Stack name setup::"
    printf "\\n\\n"
    read -r -p "Provide a stack name for use with traefik: " STACK_NAME
fi

# Configure the access to the Deluge Daemon
# The same credentials can be used for NZBGet's webui
if [[ -z "$DAEMONNAME" || -z "$DAEMONANS" || "$DAEMONANS" == "y" ]]; then
    echo "You need to set a username and password for a few of the programs - including"
    echo "the Deluge daemon, and NZBGet's API & web interface"
    read -r -p "What would you like to use as the access username?: " DAEMONNAME
    read -r -s -p "What would you like to use as the access password?: " DAEMONPASS
    printf "\\n\\n"
fi

# Get the info for the style of Portainer to use
read -r -p "Which style of Portainer do you want to use? By default 'No Auth' will be used. (noauth, auth): " portainerstyle
if [ -z "$portainerstyle" ]; then
   portainerstyle=--no-auth
elif [ "$portainerstyle" == "noauth" ]; then
   portainerstyle=--no-auth
elif [ "$portainerstyle" == "auth" ]; then
   portainerstyle=
fi

# Ask user if they already have TV, Movie, and Music directories
if [[ -z "$DLDIRECTORY" || -z "$DIRANSWER" || "$DIRANSWER" == "y"  ]]; then
printf "\\n\\n"
printf "If you already have TV - Movie - Music directories you want to use you can enter them next.\\n"
printf "If you want htpc-docker-standup to generate it's own directories just press enter to these questions."
printf "\\n\\n"
read -r -p "Where do you store your DOWNLOADS? (Please use full path - /path/to/downloads ): " DLDIRECTORY
read -r -p "Where do you store your TV media? (Please use full path - /path/to/tv ): " TVDIRECTORY
read -r -p "Where do you store your MOVIE media? (Please use full path - /path/to/movies ): " MOVIEDIRECTORY
read -r -p "Where do you store your MUSIC media? (Please use full path - /path/to/music ): " MUSICDIRECTORY
fi

# Create the directory structure
if [ -z "$DLDIRECTORY" ]; then
    mkdir -p content/completed
    mkdir -p content/incomplete
    DLDIRECTORY="$PWD/content"
else
  mkdir -p "$DLDIRECTORY"/completed
  mkdir -p "$DLDIRECTORY"/incomplete
fi
if [ -z "$TVDIRECTORY" ]; then
    mkdir -p content/tv
    TVDIRECTORY="$PWD/content/tv"
fi
if [ -z "$MOVIEDIRECTORY" ]; then
    mkdir -p content/movies
    MOVIEDIRECTORY="$PWD/content/movies"
fi
if [ -z "$MUSICDIRECTORY" ]; then
    mkdir -p content/music
    MUSICDIRECTORY="$PWD/content/music"
fi

# Create the .env file
echo "Creating the .env file with the values we have gathered"
printf "\\n"
echo "
### 
###  -----------------------------------------------------
###  H T P C - D O C K E R   C O N F I G   S E T T I N G S
###  -----------------------------------------------------
###  The values configured here are applied during
###  $ docker-compose up -d
###  -----------------------------------------------------
###     DOCKER-COMPOSE ENVIRONMENT VARIABLES BEGIN HERE
###  -----------------------------------------------------
###
LOCALUSER=$LOCALUSER
HOSTNAME=$HOSTNAME
IP_ADDRESS=$IP_ADDRESS
PUID=$PUID
PGID=$PGID
PMSTOKEN=$PMSTOKEN
VPNPROVIDER=pia
VPNUNAME=$VPNUNAME
VPNPASS=$VPNPASS
VPN_REMOTE=$VPN_REMOTE
CIDR_ADDRESS=$CIDR_ADDRESS
TZ=$TZ
PMSTAG=$PMSTAG
EMAIL=$EMAIL
DOMAIN=$DOMAIN
WATCHTOWER_EMAIL=$WATCHTOWER_EMAIL
SMTP_FROM=$SMTP_FROM
SMTP_SERVER=$SMTP_SERVER
SMTP_PORT=$SMTP_PORT
SMTP_USER=$SMTP_USER
SMTP_PASS=$SMTP_PASS
SPEEDTEST_INTERVAL=300
TRAEFIK_AUTH=$TRAEFIK_AUTH
STACK_NAME=$STACK_NAME
DLDIRECTORY=$DLDIRECTORY
TVDIRECTORY=$TVDIRECTORY
MOVIEDIRECTORY=$MOVIEDIRECTORY
MUSICDIRECTORY=$MUSICDIRECTORY
CONFIGDIR=$CONFIGDIR/" >> .env
echo ".env file creation complete!"
printf "\\n\\n"

# Move back-up .env files
mv 20*.env historical/env_files/ > /dev/null 2>&1
mv historical/20*.env historical/env_files/ > /dev/null 2>&1

# Download & Launch the containers
echo "The containers will now be pulled and launched"
echo "This may take a while depending on your download speed"
read -r -p "Press any key to continue... " -n1 -s
printf "\\n\\n"
export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120
docker-compose up -d --remove-orphans
printf "\\n\\n"

# Finish up the config
printf "Configuring DelugeVPN, NZBGet, and Permissions \\n"
printf "This may take a few minutes...\\n\\n"

# Configure DelugeVPN: Set Daemon access on, delete the core.conf~ file
while [ ! -f $CONFIGDIR/delugevpn/config/core.conf ]; do sleep 1; done
docker stop delugevpn > /dev/null 2>&1
rm $CONFIGDIR/delugevpn/config/core.conf~ > /dev/null 2>&1
perl -i -pe 's/"allow_remote": false,/"allow_remote": true,/g'  $CONFIGDIR/delugevpn/config/core.conf
perl -i -pe 's/"move_completed": false,/"move_completed": true,/g'  $CONFIGDIR/delugevpn/config/core.conf
docker start delugevpn > /dev/null 2>&1

# Configure NZBGet
while [ ! -f $CONFIGDIR/nzbget/nzbget.conf ]; do sleep 1; done
docker stop nzbget > /dev/null 2>&1
perl -i -pe "s/ControlUsername=nzbget/ControlUsername=$DAEMONNAME/g"  $CONFIGDIR/nzbget/nzbget.conf
perl -i -pe "s/ControlPassword=tegbzn6789/ControlPassword=$DAEMONPASS/g"  $CONFIGDIR/nzbget/nzbget.conf
perl -i -pe "s/{MainDir}\/intermediate/{MainDir}\/incomplete/g" $CONFIGDIR/nzbget/nzbget.conf
docker start nzbget > /dev/null 2>&1

# Push the Deluge Daemon, NZBGet Access, and Minio info the to Auth file and the .env file
echo "$DAEMONNAME":"$DAEMONPASS":10 >> $CONFIGDIR/delugevpn/config/auth
{
echo "DAEMONNAME=$DAEMONNAME"
echo "DAEMONPASS=$DAEMONPASS"
} >> .env

echo "The stack has been set up. You can access it by moving to each of the containers frontend domains."
echo "Don't forget to change your settings in the varken.ini file, for more ifnfo see github.com/Boerderij/Varken"